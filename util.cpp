
#include "util.hpp"

JsonObject & buildResult(String call, int error, String data){
  const size_t jsonResultBufferSize = JSON_OBJECT_SIZE(2) + (30 + call.length() / 10) + (30 + data.length() / 10) ;
  DynamicJsonBuffer jsonResultBuffer(jsonResultBufferSize);
  JsonObject& result = jsonResultBuffer.createObject();
  result["call"] = call;
  result["error"] = error;
  if(data != ""){
    result["data"] = data;
  }
  return result;
}

void initLed(){
  pinMode(LED, OUTPUT);
  led(false);
}

void led(bool state){
  if(INVERTED) digitalWrite(LED, state ? LOW : HIGH);
  else digitalWrite(LED, state ? HIGH : LOW);
}

void longBlink(int n){
  for(int i=0;i<n;i++){
    led(true);
    delay(500);
    led(false);
    delay(500);
  }
}

void shortBlink(int n){
  for(int i=0;i<n;i++){
    led(true);
    delay(50);
    led(false);
    delay(50);
  }
}
