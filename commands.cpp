#include "commands.hpp"

int procConnectResponse(String response){
  StaticJsonBuffer<4096> jsonBuffer;
  JsonObject & jsonRespose = jsonBuffer.parseObject(response);
  if (jsonRespose.success() == false) {
    return -100;
  }
  if (jsonRespose.containsKey("call") == false) {
    return -100;
  }
  if (jsonRespose.containsKey("error") == false) {
    return -100;
  }
  if (jsonRespose["call"].as<String>() != "connect"){
    return -100;
  }
  return jsonRespose["error"].as<int>();
}

int procRestResponse(String response){
  StaticJsonBuffer<4096> jsonBuffer;
  JsonObject & jsonRespose = jsonBuffer.parseObject(response);
  if (jsonRespose.success() == false) {
    return -100;
  }
  if (jsonRespose.containsKey("call") == false) {
    return -100;
  }
  if (jsonRespose.containsKey("error") == false) {
    return -100;
  }
  if (jsonRespose["call"].as<String>() != "rest"){
    return -100;
  }
  return jsonRespose["error"].as<int>();
}


int waitForCommandResponse(callback cb, int ignoringResult, int timeout = 20){
  String inputString = "";
  int commandResult = ignoringResult;
  for (int i = 0; i < timeout && commandResult == ignoringResult; i++) {
    delay(1000);
    while (Serial.available()) {
      char inChar = (char)Serial.read();
      inputString += inChar;
      if (inChar == '\n') {
        commandResult = (int)cb(inputString);
        inputString = "";
      }
    }
  }
  return  commandResult;
}

int connectWifi(){
  StaticJsonBuffer<1024> jsonBuffer;
  JsonObject & jsonCmd = jsonBuffer.createObject();
  jsonCmd["call"] = "connect";
  jsonCmd["essid"] = WIFI_ESSID;
  jsonCmd["password"] = WIFI_PASSWORD;
  jsonCmd.printTo(Serial);
  Serial.println();
  return waitForCommandResponse(&procConnectResponse, -100);
}

int attackReprt(int attackAttempts, String attackedMac, String targetMac){
  StaticJsonBuffer<2500> attackReprtBuffer;
  StaticJsonBuffer<1024> headersBuffer;
  StaticJsonBuffer<1024> bodyBuffer;
  JsonObject & jsonCmd = attackReprtBuffer.createObject();
  jsonCmd["call"] = "rest";
  jsonCmd["ssl"] = REST_SSL;
  jsonCmd["host"] = REST_HOST;
  jsonCmd["port"] = REST_PORT;
  jsonCmd["hostName"] = REST_HOST_NAME;
  jsonCmd["method"] = REST_METHOD;
  jsonCmd["path"] = REST_PATH;
  JsonArray & restHeaders = headersBuffer.parseArray(REST_HEADERS);
  jsonCmd["headers"] = restHeaders;
  JsonObject & restBody = bodyBuffer.createObject();
  restBody["attackAttempts"] = attackAttempts;
  restBody["attackedMac"] = attackedMac;
  restBody["targetMac"] = targetMac;
  String body = "";
  restBody.printTo(body);
  jsonCmd["body"] = body;
  jsonCmd.printTo(Serial);
  Serial.println();
  return waitForCommandResponse(&procRestResponse, -100);
}
