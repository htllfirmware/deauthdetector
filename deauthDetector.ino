#include <ESP8266WiFi.h>
#include "Mac.h"
#include "commands.hpp"
#include "util.hpp"

extern "C" {
  #include "user_interface.h"
}

//===== SETTINGS =====//
#define channel 1 //the channel it should scan on (1-14)
#define channelHopping true //scan on all channels
#define maxChannel 13 //US = 11, EU = 13, Japan = 14
#define ledPin 2 //led pin ( 2 = built-in LED)
#define inverted true // invert HIGH/LOW for the LED
#define packetRate 1 //min. packets before it gets recognized as an attack

#define scanTime 500 //scan time per channel in ms

#define MAX_ATTACKS_PER_REPORT 3
#define MAX_INT 32767

Mac * from;
Mac * to;
int attackCount = 0;
unsigned long prevTime = 0;
unsigned long curTime = 0;
int curChannel = channel;
bool wifiConnected = false;

void sniffer(uint8_t *buf, uint16_t len) {
  if(len>27){
    if(buf[12] == 0xA0 || buf[12] == 0xC0){
      from->set(buf[16],buf[17],buf[18],buf[19],buf[20],buf[21]);
      to->set(buf[22],buf[23],buf[24],buf[25],buf[26],buf[27]);
      if(attackCount < MAX_INT) attackCount++;
    }
  }
}

void setup() {
  Serial.begin(115200);
  while (!Serial) continue;
  prevTime = millis() - scanTime;
  from = new Mac;
  to = new Mac;
  wifi_set_opmode(STATION_MODE);
  wifi_promiscuous_enable(0);
  WiFi.disconnect();
  wifi_set_promiscuous_rx_cb(sniffer);
  wifi_set_channel(curChannel);
  wifi_promiscuous_enable(1);

  initLed();
  Serial.println();

}

void loop() {
  curTime = millis();
  if(wifiConnected == false) {
    if(connectWifi() == 0){
      wifiConnected = true;
      longBlink(2);
    } else {
      shortBlink(5);
    }
  }
  //disconnect emulation
  // if(curTime % 31337 == 0) wifiConnected = false;
  if(curTime - prevTime >= scanTime || curTime < prevTime){
    prevTime = curTime;
    if(attackCount >= packetRate){
      int attackReportResult = attackReprt(attackCount, from->toString(), to->toString());
      if(attackReportResult != 0) {
        shortBlink(10);
      }
    }
    attackCount = 0;
    if(channelHopping){
      curChannel++;
      if(curChannel > maxChannel) curChannel = 1;
      wifi_set_channel(curChannel);
    }
  }

}
