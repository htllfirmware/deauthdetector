#ifndef COMMANDS_H
#define COMMANDS_H
  #include <Arduino.h>
  #include <ArduinoJson.h>
  #include "util.hpp"
  #include "util.hpp"
  #include "creds.h"
  typedef int (*callback)(String);
  // creds.h
  // #define WIFI_ESSID "ESSID"
  // #define WIFI_PASSWORD "PASS"
  // #define REST_SSL false
  // #define REST_HOST "192.168.1.162"
  // #define REST_PORT 10443
  // #define REST_HOST_NAME "my.host"
  // #define REST_METHOD "POST"
  // #define REST_PATH "/PATH"
  // #define REST_HEADERS {"AUTH: 31337", "X-X-X: true"}

  /*
  send wifi connect command to serial port and wait for response
  @returns
    int error code;
    0 ok
    -10 connection error
    }
   */
  int connectWifi();
  /*
  send rest command to serial port and wait for response
  @returns
    int error code;
    0 ok
    -1 connection error
    -2 wrong JSON argument
    -3 wifi is not connected
    }
   */
  int attackReprt(int attackAttempts, String attackedMac, String targetMac);
#endif
